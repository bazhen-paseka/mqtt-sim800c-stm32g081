/*
 * Local_config.h
 *
 *  Created on: Dec 18, 2023
 *      Author: paseka
 */

#ifndef LOCAL_CONFIG_H_
#define LOCAL_CONFIG_H_

	#define SOFT_VERSION 	60
	#define CUBEMX_VERSION 	161
	#define		MQTT
	#define		COMP
	//#define		NOUT

	#define 	LIFECELL

#endif /* LOCAL_CONFIG_H_ */
